import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { defineMessages } from 'react-intl';

// import { withModalMounter } from '/imports/ui/components/common/modal/service';
// import Modal from '/imports/ui/components/modal/simple/component';
import Button from '/imports/ui/components/common/button/component';
import { notify } from '/imports/ui/services/notification';
import { Session } from 'meteor/session';
import { injectIntl } from 'react-intl';
import { styles } from './styles';
import Styled from './styles'
import Auth from '/imports/ui/services/auth';

const {inviteApiUrl} = Meteor.settings.public.app;
const intlMessages = defineMessages({
  webcamSettingsTitle: {
    id: 'app.videoPreview.webcamSettingsTitle',
    description: 'Title for the video preview modal',
  }
});

class AddPersonModal extends Component {
  constructor(props) {
    super(props);


    this.state = {
        invitees_name: '',
        invitees_email: '',
        email: '',
        disableButton: false,
    };

    this.updateEmail = this.updateEmail.bind(this);
    this.updateInviteesEmail = this.updateInviteesEmail.bind(this);
    this.updateInviteesName = this.updateInviteesName.bind(this);
    this.addContact = this.addContact.bind(this);
    this.copyCodeToClipboard = this.copyCodeToClipboard.bind(this);
    this.copyInvitationClipboard = this.copyInvitationClipboard.bind(this);
    this.copyPasscodeClipboard = this.copyPasscodeClipboard.bind(this);
  }

  componentDidMount(){
    Session.set("addPersonModal", "true")
  }

  updateInviteesName(ev) {
    this.setState({ invitees_name: ev.target.value });
  }

  updateInviteesEmail(ev) {
    this.setState({ invitees_email: ev.target.value });
  }

  updateEmail(ev) {
    this.setState({ email: ev.target.value });
  }

  addContact(){
    const { invitees_name, email } = this.state;
    const { pass_code, meetingId, meetingName, meeting_link, startDate, organizer_name, organizer_email, closeModal } = this.props;
    this.setState({ disableButton: true });
    
      

    console.log('inviteApiUrl ',inviteApiUrl);
    console.log('inviteApiUrl ================================================= ',inviteApiUrl);
    console.log('inviteApiUrl 1111111111111111111111111111111111111111111111111 ',inviteApiUrl);
    
    fetch(inviteApiUrl,{
        method: "POST",
        headers: {
          'content-type': 'application/json',
          'accept': 'application/json',
          // 'Authorization': "Bearer " + data.jwt
        },
        body: JSON.stringify({
          // "pass_code": pass_code,
          // "meeting_name": meetingName,
          // "meeting_link": meeting_link,
          // "start_date": startDate,
          // "organizer_name": organizer_name,
          // "organizer_email": organizer_email,
          //"meeting_id": Auth.meetingID,
          "meeting_id": meetingId,          
          "emails": email.split(/[ ,;]/).filter(e => e.trim() !== ""),
          //"invitees_email": invitees_email,
          "invitees_name": organizer_email
        })
      })
      .then(response => response.json())
      .then(data => {
        closeModal();
        notify(data.message, 'success', 'user');
      })
  }

  copyCodeToClipboard() {
    const el = this.textArea
    el.select()
    document.execCommand("copy")
  }

  copyInvitationClipboard() {
    // const el = this.textareainvitation
    // el.select()
    // document.execCommand("copy")
    const { startDate, pass_code, meetingName, organizer_name, organizer_email, meeting_link, meetingId } = this.props;
    let text = ''
    if(pass_code){
      // text = `You've been invited to a Flowz Video Conferencing Meeting: ${meetingName} by ${organizer_name} \n \n Join code: ${pass_code} \n \n ${meetingName} scheduled on ${startDate} \n Organised by ${organizer_name} (${organizer_email})`
      text = `${organizer_name} is inviting you to a scheduled Flowz Video Conferencing Meeting. \n \n Topic: ${meetingName} \n Time: ${startDate} \n \n Join Flowz Meeting \n ${meeting_link} \n \n Meeting ID: ${meetingId} \n Passcode: ${pass_code}`
    }else {
      // text = `You've been invited to a Flowz Video Conferencing Meeting: ${meetingName} by ${organizer_name} \n \n ${meetingName} scheduled on ${startDate} \n Organised by ${organizer_name} (${organizer_email})`
      text = `${organizer_name} is inviting you to a scheduled Flowz Video Conferencing Meeting. \n \n Topic: ${meetingName} \n Time: ${startDate} \n \n Join Flowz Meeting \n ${meeting_link} \n \n Meeting ID: ${meetingId}`
    }
    var textArea = document.createElement("textarea");
    textArea.value = text
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    textArea.remove();
  }

  copyPasscodeClipboard(){
    // const el = this.textarepasscode
    // el.select()
    // document.execCommand("copy")

    var copyText = document.getElementById("pwd_spn");
    var textArea = document.createElement("textarea");
    textArea.value = copyText.textContent;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    textArea.remove();
  }

  handleProceed=()=> {
    const { closeModal } = this.props;
    // closeModal();
  }

  renderModalContent=()=>{
    console.log('reached here also ... ')
    const { intl,closeModal, startDate, pass_code, meetingName, meeting_link, organizer_name, organizer_email, private_mode = false, privateMode = false, privatemode = false } = this.props;
    const { email, disableButton } = this.state;
    const text = `You've been invited to a Flowz Video Conferencing Meeting: ${meetingName} by ${organizer_name} \n \n Join code: ${pass_code} \n \n ${meetingName} scheduled on ${startDate} \n Organised by ${organizer_name} (${organizer_email})`

    const isPrivateMode = private_mode || privateMode || privatemode
    return (
      <>
      <Styled.Title>
        {/* {intl.formatMessage(intlMessages.webcamSettingsTitle)} */}
        Invite Users
      </Styled.Title>
        <Styled.emailBlock>
          {/* <div className={styles.person}> */}
            
            <Styled.Label htmlFor="setCam">
              Email
            </Styled.Label>
            <Styled.emailInput
              id="email"
              onChange={this.updateEmail}
              name="email"
              placeholder="Enter email Here"
              aria-describedby="exernal-video-note"
            />
          
        </Styled.emailBlock>
        <Styled.UrlBlock>
          <Styled.Label htmlFor="setCam">
              Meeting Link
          </Styled.Label>
          <Styled.copyInput
            id="UrlBlock"
            value={meeting_link == "undefined" ? "Url is not present" : meeting_link}
            name="copy"
            readOnly="true"
            ref={(textarea) => this.textArea = textarea}
          >

          </Styled.copyInput>
        </Styled.UrlBlock>
        {!isPrivateMode ? null : (
        <Styled.UrlBlock>
          <Styled.Label htmlFor="setCam">
              PassCode
          </Styled.Label>
          
          <Styled.Label htmlFor="setCam">
              {pass_code}
          </Styled.Label>
        </Styled.UrlBlock>
        ) 
        }
        <Styled.Footer>
        {/* <Styled.ActionWrapper> */}
          {isPrivateMode ? null : (
          <>
            <Button
              // className={styles.copyurl}
              color="default"
              // onClick={closeModal}
              label="Copy Url"
              onClick={() => this.copyCodeToClipboard()}
              aria-describedby="url"
              // circle
              />
            <Button
              // className={styles.copyurl}
              color="default"
              // onClick={closeModal}
              label="Copy Invitation"
              onClick={() => this.copyInvitationClipboard()}
              aria-describedby="invitation"
              // circle
            />
            </>
          )}
          {!isPrivateMode ? null : (
              <Button
                color="default"
                // onClick={closeModal}
                label="Passcode"
                onClick={() => this.copyPasscodeClipboard()}
                aria-describedby="passcode"
                // circle
              />
          )}
          <>
            <Button
              color="default"
              onClick={closeModal}
              label="Cancel"
              aria-describedby="Cancel"
              // circle
            />
            <Button
              color="primary"
              onClick={() => this.addContact()}
              disabled={disableButton || !email}
              label="Invite"

            />
          </>
        </Styled.Footer>
        
        </>
    )
  }

  render(){
    console.log('useradd modal calling ... ');
    const { setIsOpen,closeModal,
      isOpen,
      priority } = this.props;
    return (
      <Styled.addUserModal
        // onRequestClose={this.handleProceed}
        // hideBorder
        // //contentLabel={intl.formatMessage(intlMessages.webcamSettingsTitle)}
        // shouldShowCloseButton={true}
        // shouldCloseOnOverlayClick={true}
        // // isPhone={deviceInfo.isPhone}
        // data-test="inviteUserModal"

        modalName="AUDIO"
          onRequestClose={closeModal}
          data-test="audioModal"
          contentLabel="test"
          title="tttttttttt"
          {...{
            setIsOpen,
            isOpen,
            priority,
          }}
        
      >
        {this.renderModalContent()}
      </Styled.addUserModal>
    )
  }

}

export default injectIntl(AddPersonModal); // withModalMounter
