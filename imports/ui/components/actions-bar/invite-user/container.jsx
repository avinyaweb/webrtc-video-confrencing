import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
// import { withModalMounter } from '/imports/ui/components/common/modal/service';
import AddPersonModal from './component';
import Meetings from '/imports/api/meetings';
import Auth from '/imports/ui/services/auth';
import Users from '/imports/api/users';
import { Session } from 'meteor/session';
const AddPersonModalContainer = props => <AddPersonModal {...props} />;
const USER_CONFIG = Meteor.settings.public.user;
const ROLE_MODERATOR = USER_CONFIG.role_moderator;
export default withTracker(({ setIsOpen }) => { // withModalMounter
  const meeting = Meetings.findOne({ meetingId: Auth.meetingID });
  const moderator = Users.findOne({ meetingId: Auth.meetingID, role: ROLE_MODERATOR });
  const date =  new Date(meeting.durationProps.createdDate)

  console.log('useradd calling ... ');
  return({
    closeModal: () => {
      setIsOpen(false)
      // mountModal(null);
      // Session.set("addPersonModal", "false")
    },
    meetingName: meeting.meetingProp.name,
    meetingId: meeting.meetingProp?.extId?.split('-')[3],
    startDate: date.getDate() + "/"+ parseInt(date.getMonth()+1) +"/"+date.getFullYear(),
    pass_code: meeting.metadataProp?.metadata?.passcode, 
    meeting_link: meeting.metadataProp?.metadata?.joinurl, 
    organizer_name: meeting.metadataProp?.metadata?.organizername || '', 
    organizer_email: meeting.metadataProp?.metadata?.organizeremail || '', 
    privateMode: meeting.metadataProp?.metadata?.privateMode || false, 
    private_mode: meeting.metadataProp?.metadata?.private_mode || false,
    privatemode: meeting.metadataProp?.metadata?.privatemode || false,


  })
  
})(AddPersonModalContainer);

