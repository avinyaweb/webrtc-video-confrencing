import styled, { css, keyframes } from 'styled-components';
import {
  borderSize,
  borderSizeLarge,
} from '/imports/ui/stylesheets/styled-components/general';
import {
  colorGrayLabel,
  colorWhite,
  colorOffWhite,
  colorGrayLighter,
  colorGrayDark,
  colorPrimary,
  colorText,
  colorFLZGreyDark,
  colorFLZGrey
} from '/imports/ui/stylesheets/styled-components/palette';
import {
  fontSizeLarge,
  fontSizeBase,
  lineHeightComputed,
  headingsFontWeight,
} from '/imports/ui/stylesheets/styled-components/typography';
import { smallOnly, landscape } from '/imports/ui/stylesheets/styled-components/breakpoints';
import ModalSimple from '/imports/ui/components/common/modal/simple/component';
import ModalStyles from '/imports/ui/components/common/modal/simple/styles';


// const AudioModal = styled(ModalSimple)`
//   padding: 1rem;
//   min-height: 20rem;
// `;

const addUserModal = styled(ModalSimple)`
  padding: 1rem;
  min-height: 25rem;
  max-height: 50vh;
  background-color: ${colorGrayDark}!important;

  @media ${smallOnly} {
    height: unset;
    min-height: 22.5rem;
    max-height: unset;
  }

  ${({ isPhone }) => isPhone && `
    min-height: 100%;
    min-width: 100%;
    border-radius: 0;
  `}

  ${ModalStyles.Content} {
    flex-grow: 1;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  }
`;

const Title = styled.div`
  display: block;
  color: ${colorOffWhite};
  font-size: 1.4rem;
  text-align: center;
`;

const Label = styled.label`
  // margin-top: 8px;
  font-size: ${fontSizeBase};
  font-weight: bold;
  color: ${colorOffWhite};
  // flex-grow:1
  width:33%;
`;

const emailBlock = styled.div`
  display:flex;
  margin:5px;
  align-items: center;
`;

const UrlBlock = styled.div`
  display:flex;
  margin:5px;
  align-items: center;
`;

const copyInput = styled.input`
  flex-grow:1;
  padding:0.75rem;
  background-color: ${colorFLZGreyDark}!important;
  background-clip: padding-box;
  color: ${colorOffWhite};
  border:none;
  outline: none;
  font-size: 1rem;
  line-height: 1;
`;

const emailInput = styled.input`
  flex-grow:1;
  padding:0.75rem;
  background-color: ${colorFLZGreyDark}!important;
  background-clip: padding-box;
  color: ${colorOffWhite};
  border:none;
  outline: none;
  font-size: 1rem;
  line-height: 1;
`;

const Footer = styled.div`
  display: flex;
  justify-content: flex-end;

  > button {
    margin:5px;
  }
`;

const ActionWrapper = styled.div`
  display:flex;
`;
export default {
    addUserModal,
    emailBlock,
    emailInput,
    UrlBlock,
    copyInput,
    Label,
    Footer,
    Title,
    ActionWrapper
}