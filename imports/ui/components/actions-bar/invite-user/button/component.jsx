import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { defineMessages, injectIntl } from 'react-intl';
import Button from '/imports/ui/components/common/button/component';
import Styled from './styles';
import AddPersonModalContainer from '../container'

const propTypes = {
  intl: PropTypes.shape({
    formatMessage: PropTypes.func.isRequired,
  }).isRequired,
};
class AddUserButton extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isInviteModalOpen: false,
    };
    this.setInviteModalIsOpen = this.setInviteModalIsOpen.bind(this);
  }
  setInviteModalIsOpen(value){
    this.setState({ isInviteModalOpen: value })
  }


  render (){
    const addUserIcon=<i className='icomoon-useradd'></i>
    return (
    <Styled.Container>
    {
    <Button
        onClick={() => this.setInviteModalIsOpen(true)}
        icon='user'
        // customIcon={addUserIcon}
        label='add user' //{intl.formatMessage(vLabel)}
        color='default'
        ghost={true}
        hideLabel
        circle
        size="lg"
      />
    }
    {
      this.state.isInviteModalOpen ? <AddPersonModalContainer 
        {...{
          priority: "low",
          setIsOpen: this.setInviteModalIsOpen,
          isOpen: this.state.isInviteModalOpen
        }}
      /> : null
    }
    </Styled.Container>
    )
  }
}

AddUserButton.propTypes = propTypes;
export default injectIntl(AddUserButton); // withModalMounter
