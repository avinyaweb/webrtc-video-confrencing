import styled, { css, keyframes } from 'styled-components';
import { smallOnly } from '/imports/ui/stylesheets/styled-components/breakpoints';
import { smPaddingX, smPaddingY } from '/imports/ui/stylesheets/styled-components/general';


const Container = styled.span`
  display: flex;
  flex-flow: row;
  position: relative;

  & > div {
    position: relative;
  }
  
  & > :last-child {
    margin-left: ${smPaddingX};
    margin-right: 0;

    @media ${smallOnly} {
      margin-left: ${smPaddingY};
    }

    [dir="rtl"] & {
      margin-left: 0;
      margin-right: ${smPaddingX};

      @media ${smallOnly} {
        margin-right: ${smPaddingY};
      }
    }
  }
`;


export default {
    Container
};
