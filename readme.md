## Tech Stacks

- NodeJS
- ReactJS
- MongoDB
- Redis
- MySQL
- JavaScript
- WebRTC
- MediaSoup
- Redux
- Saas
- Meteor

## Run Project

- NPM START
- NPM RUN BUILD (to build the project for production)

## Additional Help

- [Hire webrtc expert](https://avinyaweb.com/hire-top-bigbluebutton-developer-one-of-the-best-bbb-expert/)
- [Hire ReactJS Developer](https://avinyaweb.com/)
